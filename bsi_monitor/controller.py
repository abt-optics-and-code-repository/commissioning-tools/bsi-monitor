class Controller:
    def __init__(self, gui, bSIsub):

        self.gui = gui
        self.bsi_subscriber = bSIsub
        # self.bsi_subscriber.start_subscription()

        self.bsi_subscriber.bsi_curve_signal.connect(self.on_bsi_curve_recieved)


    def on_bsi_curve_recieved(self, value):
        self.gui.set_value_curve(value)
