"""
High-level tests for the  package.

"""

import bsi_monitor


def test_version():
    assert bsi_monitor.__version__ is not None
