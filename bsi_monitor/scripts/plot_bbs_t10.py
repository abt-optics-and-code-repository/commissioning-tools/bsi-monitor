import pyjapc
import accphylib.acc_library as al
from cern_general_devices.bsi import BSI
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import matplotlib
from matplotlib.animation import FuncAnimation
from collections import deque


japc = pyjapc.PyJapc("SPS.USER.SFTPRO1")
bsi = BSI(japc)

target_t2_ex = japc.getParam(
    "rmi://virtual_sps/T2.REF/Reference#exponent", timingSelectorOverride=""
)
target_t4_ex = japc.getParam(
    "rmi://virtual_sps/T4.REF/Reference#exponent", timingSelectorOverride=""
)
target_t6_ex = japc.getParam(
    "rmi://virtual_sps/T6.REF/Reference#exponent", timingSelectorOverride=""
)

target_t2 = japc.getParam(
    "rmi://virtual_sps/T2.REF/Reference#value", timingSelectorOverride=""
)
target_t4 = japc.getParam(
    "rmi://virtual_sps/T4.REF/Reference#value", timingSelectorOverride=""
)
target_t6 = japc.getParam(
    "rmi://virtual_sps/T6.REF/Reference#value", timingSelectorOverride=""
)

data, info = bsi.getParameter()

data_time, info = bsi.get_intensity_time()

data_bsi = data[data["device_name"].str.contains("BB")]

data_bsi_t2 = data_bsi[
    data_bsi["device_name"].str.contains(".210")
    | data_bsi["device_name"].str.contains(".22")
    | data_bsi["device_name"].str.contains(".23")
]
data_bsi_t2 = data_bsi_t2.groupby(["s"]).max().iloc[:-1]
data_bsi_t4 = data_bsi[
    data_bsi["device_name"].str.contains(".210")
    | data_bsi["device_name"].str.contains(".22")
    | data_bsi["device_name"].str.contains(".240")
    | data_bsi["device_name"].str.contains(".241")
]
data_bsi_t4 = data_bsi_t4.groupby(["s"]).max().iloc[:-1]
data_bsi_t6 = data_bsi[
    data_bsi["device_name"].str.contains(".210")
    | data_bsi["device_name"].str.contains(".25")
]
data_bsi_t6 = data_bsi_t6.groupby(["s"]).max().iloc[:-1]

data_bsi_t10 = data_bsi[data_bsi["device_name"].str.contains(".04")]
bsi_t10_history = [deque(maxlen=100), deque(maxlen=100), deque(maxlen=100)]

bsi_t6_history = [deque(maxlen=100), deque(maxlen=100), deque(maxlen=100)]
bsi_to_plot = ["BSI.230949", "BSI.241149", "BSI.251010"]
bsi_dfs = [data_bsi_t2, data_bsi_t4, data_bsi_t6]


def callback(param, value):
    global data, data_bsi, data_bsi_t10, data_bsi_t2, data_bsi_t4, data_bsi_t6, data_time

    data = bsi.clean_data(value)
    data_time = bsi.clean_data_in_time(value)

    data_bsi = data[data["device_name"].str.contains("BB")]

    data_bsi_t2 = data_bsi[
        data_bsi["device_name"].str.contains(".210")
        | data_bsi["device_name"].str.contains(".22")
        | data_bsi["device_name"].str.contains(".23")
    ]
    data_bsi_t2 = data_bsi_t2.groupby(["s"]).max().iloc[:-1]
    data_bsi_t4 = data_bsi[
        data_bsi["device_name"].str.contains(".210")
        | data_bsi["device_name"].str.contains(".22")
        | data_bsi["device_name"].str.contains(".240")
        | data_bsi["device_name"].str.contains(".241")
    ]
    data_bsi_t4 = data_bsi_t4.groupby(["s"]).max().iloc[:-1]
    data_bsi_t6 = data_bsi[
        data_bsi["device_name"].str.contains(".210")
        | data_bsi["device_name"].str.contains(".25")
    ]
    data_bsi_t6 = data_bsi_t6.groupby(["s"]).max().iloc[:-1]

    bsi_dfs = [data_bsi_t2, data_bsi_t4, data_bsi_t6]
    # data_bsi_t10 = data_bsi[data_bsi["device_name"].str.contains(".04")]
    # for i, bsi_t10 in enumerate(bsi_t10_history):
    #     bsi_t10_history[i].append(data_bsi_t10.iloc[1 + i]["intensity"])
    # print(bsi_t10_history)
    for i in range(len(bsi_to_plot)):
        bsi_t6_history[i].append(
            bsi_dfs[i][bsi_dfs[i]["device_name"] == bsi_to_plot[i]]["intensity"]
        )


# data_bsi_t10 = data_bsi_t10.groupby(["s"]).max().iloc[:-1]

japc.subscribeParam(bsi.bsis_var, callback)

japc.startSubscriptions()

fig3, axis = plt.subplots(nrows=1, ncols=1)


def make_plot(i):
    global data, data_bsi, data_bsi_t10, data_bsi_t2, data_bsi_t4, data_bsi_t6, data_time
    try:
        axis.cla()

        pos1 = np.linspace(
            0,
            len(data_time.loc[data_bsi_t2.device_name.iloc[-1]].to_numpy()),
            len(data_time.loc[data_bsi_t2.device_name.iloc[-1]].to_numpy()),
        )
        pos2 = pos1 + max(pos1)
        pos3 = pos1 + max(pos2)
        pos4 = pos1 + max(pos3)

        t2_int = data_bsi_t2.iloc[-1]["intensity"] / (10 ** target_t2_ex)
        t2_int_m1 = data_bsi_t2.iloc[-2]["intensity"] / (10 ** target_t2_ex)

        t4_int = data_bsi_t4.iloc[-1]["intensity"] / (10 ** target_t4_ex)
        t4_int_m1 = data_bsi_t4.iloc[-2]["intensity"] / (10 ** target_t4_ex)

        t6_int = data_bsi_t6.iloc[-1]["intensity"] / (10 ** target_t6_ex)
        t6_int_m1 = data_bsi_t6.iloc[-2]["intensity"] / (10 ** target_t6_ex)

        t10_int = data_bsi_t10.iloc[-1]["intensity"] / (10 ** target_t6_ex)
        t10_int_m1 = data_bsi_t10.iloc[-2]["intensity"] / (10 ** target_t6_ex)
        print(data_bsi_t10)
        for ele in data_bsi_t10.device_name:
            axis.plot(
                pos1,
                data_time.loc[ele],
                label=ele,
            )

        axis.legend()

        axis.set_ylabel(fr"Intensity / $1 \times 10^{{{target_t2_ex}}}$")

        axis.set_title("BBS along P42")

    except Exception as e:
        print(e)


ani = FuncAnimation(fig3, make_plot, interval=1000)

plt.show()
