from PyQt5 import QtWidgets
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as Canvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as navTool
from matplotlib.figure import Figure
import matplotlib.pyplot as plt


#plt.style.use('dark_background')
# plt.style.use('seaborn-white')
class MplCanvas(Canvas):
    def __init__(self):
        self.fig = Figure(figsize=(8, 4))

        self.axs = self.fig.subplots(nrows=1, ncols=1)
        # self.fig.subplots_adjust(hspace=0)
#        self.axs[2] = self.fig.add_subplot(313)

        Canvas.__init__(self, self.fig)
        Canvas.setSizePolicy(self, QtWidgets.QSizePolicy.Expanding,
                             QtWidgets.QSizePolicy.Expanding)
        Canvas.updateGeometry(self)
        
    
class MplWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)   # Inherit from QWidget
        self.canvas = MplCanvas()                  # Create canvas object
        self.vbl = QtWidgets.QVBoxLayout()         # Set box for plotting
        self.vbl.addWidget(self.canvas)
        self.vbl.addWidget(navTool(self.canvas, self))
        self.setLayout(self.vbl)
