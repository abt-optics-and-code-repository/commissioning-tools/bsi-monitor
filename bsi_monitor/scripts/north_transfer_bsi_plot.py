from collections import deque

from cern_general_devices.bsi import BSI
import pyjapc
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.patches import ConnectionPatch

japc = pyjapc.PyJapc('SPS.USER.ALL')
bsi = BSI(japc)

data, info = bsi.getParameter()

data_time, info = bsi.get_intensity_time()

data_bsi = data[data["device_name"].str.contains("BSI")]

data_bsi_tt20 = data_bsi[data_bsi["device_name"].str.contains(".210")]
data_bsi_tt22 = data_bsi[data_bsi["device_name"].str.contains(".22")]
data_bsi_tt23 = data_bsi[data_bsi["device_name"].str.contains(".23")]
data_bsi_tt24 = data_bsi[data_bsi["device_name"].str.contains(".24")]
data_bsi_tt25 = data_bsi[data_bsi["device_name"].str.contains(".25")]

data_bsi_t10 = data_bsi[data_bsi["device_name"].str.contains(".04")]
bsi_t10_history = [deque(maxlen=100), deque(maxlen=100), deque(maxlen=100)]


def callback(param, value):

    # global data, data_bsi, data_bsi_t10, data_bsi_t2, data_bsi_t4, data_bsi_t6, data_time
    global data, data_bsi, data_bsi_t10, data_bsi_tt20, data_bsi_tt22, data_bsi_tt23, data_bsi_tt24, data_bsi_tt25, data_time

    data = bsi.clean_data(value)
    data_time = bsi.clean_data_in_time(value)

    data_bsi = data[data["device_name"].str.contains("BSI")]
    print(data_bsi)

    data_bsi_tt20 = data_bsi[data_bsi["device_name"].str.contains(".210")]
    data_bsi_tt20 = data_bsi_tt20.sort_values(by=['s'])
    data_bsi_tt22 = data_bsi[data_bsi["device_name"].str.contains(".22")]
    data_bsi_tt22 = data_bsi_tt22.sort_values(by=['s'])
    data_bsi_tt23 = data_bsi[data_bsi["device_name"].str.contains(".23")]
    data_bsi_tt23 = data_bsi_tt23.sort_values(by=['s'])
    data_bsi_tt24 = data_bsi[data_bsi["device_name"].str.contains(".24")]
    data_bsi_tt24 = data_bsi_tt24.sort_values(by=['s'])
    data_bsi_tt25 = data_bsi[data_bsi["device_name"].str.contains(".25")]
    data_bsi_tt25 = data_bsi_tt25.sort_values(by=['s'])
    data_bsi_t10 = data_bsi[data_bsi["device_name"].str.contains(".04")]
    data_bsi_t10 = data_bsi_t10.sort_values(by=['s'])

    print(data_bsi_tt22)


fig3 = plt.figure(constrained_layout=True, figsize=(22, 14))

main_ax = fig3.add_subplot(xticks=[], yticks=[])
title_font_size = 10

tt20_spill_ax = fig3.add_axes([.03, .85, .2, .08])
tt20_spill_ax.set_title("TT20 spill", fontsize=title_font_size)

tt20_intensity_ax = fig3.add_axes([.03, .7, .2, .08])
tt20_intensity_ax.set_title("TT20 intensity", fontsize=title_font_size)

tt22_spill_ax = fig3.add_axes([.28, .85, .2, .08])
tt22_spill_ax.set_title("TT22 spill", fontsize=title_font_size)

tt22_intensity_ax = fig3.add_axes([.28, .7, .2, .08])
tt22_intensity_ax.set_title("TT22 intensity", fontsize=title_font_size)

tt23_spill_ax = fig3.add_axes([.53, .85, .2, .08])
tt23_spill_ax.set_title("TT23 spill", fontsize=title_font_size)

tt23_intensity_ax = fig3.add_axes([.53, .7, .2, .08])
tt23_intensity_ax.set_title("TT23 intensity", fontsize=title_font_size)

tt24_spill_ax = fig3.add_axes([.53, .5, .2, .08])
tt24_spill_ax.set_title("TT24 spill", fontsize=title_font_size)

tt24_intensity_ax = fig3.add_axes([.53, .35, .2, .08])
tt24_intensity_ax.set_title("TT24 intensity", fontsize=title_font_size)

tt25_spill_ax = fig3.add_axes([.4, .2, .2, .08])
tt25_spill_ax.set_title("TT25 spill", fontsize=title_font_size)

tt25_intensity_ax = fig3.add_axes([.4, 0.05, .2, .08])
tt25_intensity_ax.set_title("TT25 intensity", fontsize=title_font_size)

p42_spill_ax = fig3.add_axes([.78, .5, .2, .08])
p42_spill_ax.set_title("P42 spill", fontsize=title_font_size)

p42_intensity_ax = fig3.add_axes([.78, 0.35, .2, .08])
p42_intensity_ax.set_title("P42 intensity", fontsize=title_font_size)

main_ax.text(0.25, .81, 'SPL1', fontsize=14, fontweight='bold')
main_ax.text(0.5, .81, 'SPL2', fontsize=14, fontweight='bold')
main_ax.text(0.75, .81, 'T2', fontsize=14, fontweight='bold')
main_ax.text(0.75, .51, 'T4', fontsize=14, fontweight='bold')
main_ax.text(0.75, .21, 'T6', fontsize=14, fontweight='bold')

coordsA = "data"
coordsB = "data"

tt20_start_point = (0.05, 0.8)
tt20_end_point = (0.25, 0.8)

tt22_start_point = tt20_end_point
tt22_end_point = (0.5, 0.8)

tt23_start_point = tt22_end_point
tt23_end_point = (0.75, 0.8)

tt24_vert_start_point = tt22_end_point
tt24_vert_end_point = (0.5, 0.45)

tt24_start_point = tt24_vert_end_point
tt24_end_point = (0.75, 0.45)

tt25_vert_start_point = tt20_end_point
tt25_vert_end_point = (0.25, 0.15)

tt25_start_point = tt25_vert_end_point
tt25_end_point = (0.75, 0.15)

p42_start_point = tt24_end_point
p42_end_point = (0.95, 0.45)

tt20 = ConnectionPatch(tt20_start_point, tt20_end_point, coordsA, coordsB,
                       arrowstyle="-|>", shrinkA=0, shrinkB=0,
                       mutation_scale=20, fc="w")
main_ax.add_artist(tt20)

tt22 = ConnectionPatch(tt22_start_point, tt22_end_point, coordsA, coordsB,
                       arrowstyle="-|>", shrinkA=0, shrinkB=0,
                       mutation_scale=20, fc="w")
main_ax.add_artist(tt22)

tt23 = ConnectionPatch(tt23_start_point, tt23_end_point, coordsA, coordsB,
                       arrowstyle="-|>", shrinkA=0, shrinkB=0,
                       mutation_scale=20, fc="w")
main_ax.add_artist(tt23)

tt24 = ConnectionPatch(tt24_start_point, tt24_end_point, coordsA, coordsB,
                       arrowstyle="-|>", shrinkA=0, shrinkB=0,
                       mutation_scale=20, fc="w")
main_ax.add_artist(tt24)

p42 = ConnectionPatch(p42_start_point, p42_end_point, coordsA, coordsB,
                      arrowstyle="-|>", shrinkA=0, shrinkB=0,
                      mutation_scale=20, fc="w")
main_ax.add_artist(p42)

tt24_vert = ConnectionPatch(tt24_vert_start_point, tt24_vert_end_point, coordsA, coordsB,
                            shrinkA=0, shrinkB=0,
                            mutation_scale=20, fc="w")
main_ax.add_artist(tt24_vert)

tt25_vert = ConnectionPatch(tt25_vert_start_point, tt25_vert_end_point, coordsA, coordsB,
                            shrinkA=0, shrinkB=0,
                            mutation_scale=20, fc="w")
main_ax.add_artist(tt25_vert)

tt25 = ConnectionPatch(tt25_start_point, tt25_end_point, coordsA, coordsB,
                       arrowstyle="-|>", shrinkA=0, shrinkB=0,
                       mutation_scale=20, fc="w")

main_ax.add_artist(tt25)


def make_plot(i):
    global data, data_bsi, data_bsi_t10, data_bsi_tt20, data_bsi_tt22, data_bsi_tt23, data_bsi_tt24, data_bsi_tt25, data_time

    # for i, ele in enumerate(data_bsi_t10.device_name):
    #     p42_spill_ax.plot(
    #         data_time.loc[ele], color=s_m.to_rgba(data_bsi_t10.index[i])
    #     )
    tt20_spill_ax.clear()
    tt20_intensity_ax.clear()
    tt22_spill_ax.clear()
    tt22_intensity_ax.clear()
    tt23_spill_ax.clear()
    tt23_intensity_ax.clear()
    tt24_spill_ax.clear()
    tt24_intensity_ax.clear()
    tt25_spill_ax.clear()
    tt25_intensity_ax.clear()
    p42_spill_ax.clear()
    p42_intensity_ax.clear()

    colours = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'b', 'g', 'r', 'c', 'm', 'y', 'k']

    for i, ele in enumerate(data_bsi_tt20.device_name):
        tt20_spill_ax.plot(data_time.loc[ele], color=colours[i])
    for i, (intensity, position) in enumerate(zip(data_bsi_tt20.intensity, data_bsi_tt20.s)):
        tt20_intensity_ax.scatter(position, intensity, color=colours[i])
        tt20_intensity_ax.set_xlim(left=-3)
        tt20_intensity_ax.set_ylim(bottom=0)

    for i, ele in enumerate(data_bsi_tt22.device_name):
        tt22_spill_ax.plot(data_time.loc[ele], color=colours[i])
    for i, (intensity, position) in enumerate(zip(data_bsi_tt22.intensity, data_bsi_tt22.s)):
        tt22_intensity_ax.scatter(position, intensity, color=colours[i])
        tt22_intensity_ax.set_xlim(left=-3)
        tt22_intensity_ax.set_ylim(bottom=0)

    for i, ele in enumerate(data_bsi_tt23.device_name):
        tt23_spill_ax.plot(data_time.loc[ele], color=colours[i])
    for i, (intensity, position) in enumerate(zip(data_bsi_tt23.intensity, data_bsi_tt23.s)):
        tt23_intensity_ax.scatter(position, intensity, color=colours[i])
        tt23_intensity_ax.set_xlim(left=-3)
        tt23_intensity_ax.set_ylim(bottom=0)

    for i, ele in enumerate(data_bsi_tt24.device_name):
        tt24_spill_ax.plot(data_time.loc[ele], color=colours[i])
    for i, (intensity, position) in enumerate(zip(data_bsi_tt24.intensity, data_bsi_tt24.s)):
        tt24_intensity_ax.scatter(position, intensity, color=colours[i])
        tt24_intensity_ax.set_xlim(left=-3)
        tt24_intensity_ax.set_ylim(bottom=0)

    for i, ele in enumerate(data_bsi_tt25.device_name):
        tt25_spill_ax.plot(data_time.loc[ele], color=colours[i])
    for i, (intensity, position) in enumerate(zip(data_bsi_tt25.intensity, data_bsi_tt25.s)):
        tt25_intensity_ax.scatter(position, intensity, color=colours[i])
        tt25_intensity_ax.set_xlim(left=-3)
        tt25_intensity_ax.set_ylim(bottom=0)

    for i, ele in enumerate(data_bsi_t10.device_name):
        p42_spill_ax.plot(data_time.loc[ele], color=colours[i])
    for i, (intensity, position) in enumerate(zip(data_bsi_t10.intensity, data_bsi_t10.s)):
        p42_intensity_ax.scatter(position, intensity, color=colours[i])
        p42_intensity_ax.set_xlim(left=-3)
        p42_intensity_ax.set_ylim(bottom=0)


japc.subscribeParam(bsi.bsis_var, callback)

japc.startSubscriptions()

ani = FuncAnimation(fig3, make_plot, interval=1000)

plt.show()