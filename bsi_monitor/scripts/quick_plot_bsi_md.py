import pyjapc
# import accphylib.acc_library as al
from cern_general_devices.bsi import BSI
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import matplotlib
from matplotlib.animation import FuncAnimation
from collections import deque


japc = pyjapc.PyJapc("SPS.USER.SFTPRO1")
bsi = BSI(japc)

target_t2_ex = japc.getParam(
    "rmi://virtual_sps/T2.REF/Reference#exponent", timingSelectorOverride=""
)
target_t4_ex = japc.getParam(
    "rmi://virtual_sps/T4.REF/Reference#exponent", timingSelectorOverride=""
)
target_t6_ex = japc.getParam(
    "rmi://virtual_sps/T6.REF/Reference#exponent", timingSelectorOverride=""
)

target_t2 = japc.getParam(
    "rmi://virtual_sps/T2.REF/Reference#value", timingSelectorOverride=""
)
target_t4 = japc.getParam(
    "rmi://virtual_sps/T4.REF/Reference#value", timingSelectorOverride=""
)
target_t6 = japc.getParam(
    "rmi://virtual_sps/T6.REF/Reference#value", timingSelectorOverride=""
)

data, info = bsi.getParameter()

data_time, info = bsi.get_intensity_time()

data_bsi = data[data["device_name"].str.contains("BSI")]

data_bsi_t2 = data_bsi[
    data_bsi["device_name"].str.contains(".210")
    | data_bsi["device_name"].str.contains(".22")
    | data_bsi["device_name"].str.contains(".23")
]
data_bsi_t2 = data_bsi_t2.groupby(["s"]).max().iloc[:-1]
data_bsi_t4 = data_bsi[
    data_bsi["device_name"].str.contains(".210")
    | data_bsi["device_name"].str.contains(".22")
    | data_bsi["device_name"].str.contains(".240")
    | data_bsi["device_name"].str.contains(".241")
]
data_bsi_t4 = data_bsi_t4.groupby(["s"]).max().iloc[:-1]
data_bsi_t6 = data_bsi[
    data_bsi["device_name"].str.contains(".210")
    | data_bsi["device_name"].str.contains(".25")
]
data_bsi_t6 = data_bsi_t6.groupby(["s"]).max().iloc[:-1]

data_bsi_t10 = data_bsi[data_bsi["device_name"].str.contains(".04")]
bsi_t10_history = [deque(maxlen=100), deque(maxlen=100), deque(maxlen=100)]

bsi_t6_history = [deque(maxlen=100), deque(maxlen=100), deque(maxlen=100)]
bsi_to_plot = ["BSI.230949", "BSI.241149", "BSI.251010"]
bsi_dfs = [data_bsi_t2, data_bsi_t4, data_bsi_t6]


def callback(param, value):
    global data, data_bsi, data_bsi_t10, data_bsi_t2, data_bsi_t4, data_bsi_t6, data_time

    data = bsi.clean_data(value)
    data_time = bsi.clean_data_in_time(value)

    data_bsi = data[data["device_name"].str.contains("BSI")]

    data_bsi_t2 = data_bsi[
        data_bsi["device_name"].str.contains(".210")
        | data_bsi["device_name"].str.contains(".22")
        | data_bsi["device_name"].str.contains(".23")
    ]
    data_bsi_t2 = data_bsi_t2.groupby(["s"]).max().iloc[:-1]
    data_bsi_t4 = data_bsi[
        data_bsi["device_name"].str.contains(".210")
        | data_bsi["device_name"].str.contains(".22")
        | data_bsi["device_name"].str.contains(".240")
        | data_bsi["device_name"].str.contains(".241")
    ]
    data_bsi_t4 = data_bsi_t4.groupby(["s"]).max().iloc[:-1]
    data_bsi_t6 = data_bsi[
        data_bsi["device_name"].str.contains(".210")
        | data_bsi["device_name"].str.contains(".25")
    ]
    data_bsi_t6 = data_bsi_t6.groupby(["s"]).max().iloc[:-1]

    bsi_dfs = [data_bsi_t2, data_bsi_t4, data_bsi_t6]
    # data_bsi_t10 = data_bsi[data_bsi["device_name"].str.contains(".04")]
    # for i, bsi_t10 in enumerate(bsi_t10_history):
    #     bsi_t10_history[i].append(data_bsi_t10.iloc[1 + i]["intensity"])
    # print(bsi_t10_history)
    for i in range(len(bsi_to_plot)):
        bsi_t6_history[i].append(
            bsi_dfs[i][bsi_dfs[i]["device_name"] == bsi_to_plot[i]]["intensity"]
        )


# data_bsi_t10 = data_bsi_t10.groupby(["s"]).max().iloc[:-1]

japc.subscribeParam(bsi.bsis_var, callback)

japc.startSubscriptions()

fig3 = plt.figure(constrained_layout=True, figsize=(7, 9))
gs = fig3.add_gridspec(7, 6)
f3_ax4 = fig3.add_subplot(gs[-2:, :])
f3_ax1 = fig3.add_subplot(gs[0:2, :])
f3_ax2 = fig3.add_subplot(gs[2:5, :3])
f3_ax31 = fig3.add_subplot(gs[2, 3:])
f3_ax32 = fig3.add_subplot(gs[3, 3:])
f3_ax33 = fig3.add_subplot(gs[4, 3:], sharex=f3_ax31)


def make_plot(i):
    global data, data_bsi, data_bsi_t10, data_bsi_t2, data_bsi_t4, data_bsi_t6, data_time
    try:

        [ax.cla() for ax in fig3.get_axes()]

        f3_ax4.set_title("Splitting ratios")

        # p0 = f3_ax4.bar("T2", data_bsi_t2.iloc[-1]["intensity"] / (10 ** target_t2_ex))
        # p1 = f3_ax4.bar("T4", data_bsi_t4.iloc[-1]["intensity"] / (10 ** target_t4_ex))
        # p2 = f3_ax4.bar("T6", data_bsi_t6.iloc[-1]["intensity"] / (10 ** target_t6_ex))

        pos1 = np.linspace(
            0,
            len(data_time.loc[data_bsi_t2.device_name.iloc[-1]].to_numpy()),
            len(data_time.loc[data_bsi_t2.device_name.iloc[-1]].to_numpy()),
        )
        pos2 = pos1 + max(pos1)
        pos3 = pos1 + max(pos2)
        pos4 = pos1 + max(pos3)

        t2_int = data_bsi_t2.iloc[-1]["intensity"] / (10 ** target_t2_ex)
        t2_int_m1 = data_bsi_t2.iloc[-2]["intensity"] / (10 ** target_t2_ex)

        p0 = f3_ax4.plot(
            pos1,
            data_time.loc[data_bsi_t2.device_name.iloc[-2]]
            / max(data_time.loc[data_bsi_t2.device_name.iloc[-2]])
            * t2_int_m1,
            label="T2",
        )
        f3_ax4.fill_between(
            pos1,
            0,
            data_time.loc[data_bsi_t2.device_name.iloc[-1]]
            / max(data_time.loc[data_bsi_t2.device_name.iloc[-1]])
            * t2_int,
            color=p0[0].get_color(),
            alpha=0.4,
        )

        t4_int = data_bsi_t4.iloc[-1]["intensity"] / (10 ** target_t4_ex)
        t4_int_m1 = data_bsi_t4.iloc[-2]["intensity"] / (10 ** target_t4_ex)

        p1 = f3_ax4.plot(
            pos2,
            data_time.loc[data_bsi_t4.device_name.iloc[-2]]
            / max(data_time.loc[data_bsi_t4.device_name.iloc[-2]])
            * t4_int_m1,
            label="T4",
        )
        f3_ax4.fill_between(
            pos2,
            0,
            data_time.loc[data_bsi_t4.device_name.iloc[-1]]
            / max(data_time.loc[data_bsi_t4.device_name.iloc[-1]])
            * t4_int,
            color=p1[0].get_color(),
            alpha=0.4,
        )

        t6_int = data_bsi_t6.iloc[-1]["intensity"] / (10 ** target_t6_ex)
        t6_int_m1 = data_bsi_t6.iloc[-2]["intensity"] / (10 ** target_t6_ex)

        p2 = f3_ax4.plot(
            pos3,
            data_time.loc[data_bsi_t6.device_name.iloc[-2]]
            / max(data_time.loc[data_bsi_t6.device_name.iloc[-2]])
            * t6_int_m1,
            label="T6",
        )
        f3_ax4.fill_between(
            pos3,
            0,
            data_time.loc[data_bsi_t6.device_name.iloc[-1]]
            / max(data_time.loc[data_bsi_t6.device_name.iloc[-1]])
            * t6_int,
            color=p2[0].get_color(),
            alpha=0.4,
        )

        t10_int = data_bsi_t10.iloc[-1]["intensity"] / (10 ** target_t6_ex)
        t10_int_m1 = data_bsi_t10.iloc[-2]["intensity"] / (10 ** target_t6_ex)

        f3_ax4.plot(
            pos4,
            data_time.loc[data_bsi_t10.device_name.iloc[-3]]
            / max(data_time.loc[data_bsi_t10.device_name.iloc[-3]])
            * t10_int_m1,
            label="T10-1",
        )
        p3 = f3_ax4.plot(
            pos4,
            data_time.loc[data_bsi_t10.device_name.iloc[-2]]
            / max(data_time.loc[data_bsi_t10.device_name.iloc[-2]])
            * t10_int_m1,
            label="T10-2",
        )
        p3 = f3_ax4.plot(
            pos4,
            data_time.loc[data_bsi_t10.device_name.iloc[-1]]
            / max(data_time.loc[data_bsi_t10.device_name.iloc[-1]])
            * t10_int_m1,
            label="T10-3",
        )
        # f3_ax4.fill_between(
        #     pos4,
        #     0,
        #     data_time.loc[data_bsi_t10.device_name.iloc[-1]]
        #     / max(data_time.loc[data_bsi_t10.device_name.iloc[-1]])
        #     * t10_int,
        #     color=p3[0].get_color(),
        #     alpha=0.4,
        # )

        # f3_ax4.axhline(target_t2, 0, 0.25, ls="--", c=p0[0].get_color())
        # f3_ax4.axhline(target_t4, 0.25, 0.5, ls="--", c=p1[0].get_color())
        # f3_ax4.axhline(target_t6, 0.5, 0.75, ls="--", c=p2[0].get_color())

        f3_ax4.legend()

        f3_ax4.set_ylabel(fr"Intensity / $1 \times 10^{{{target_t2_ex}}}$")

        f3_ax1.set_title("BSI along TT20")
        # f3_ax1.set_axis_off()
        for i, bsi_t6_ in enumerate(bsi_t6_history):

            f3_ax1.plot(bsi_t6_, label=bsi_to_plot[i])

        f3_ax1.legend()
        # f3_ax1.plot(data_bsi_t2.index, np.zeros(len(data_bsi_t2)) + 3)
        # [f3_ax1.text(s, 2, ele_name, rotation=45, fontsize=7) for s, ele_name in zip(data_bsi_t2.index, data_bsi_t2.device_name)]

        # f3_ax1.plot(data_bsi_t4.index, np.zeros(len(data_bsi_t4)))
        # [f3_ax1.text(s, -1, ele_name, rotation=45, fontsize=7) for s, ele_name in zip(data_bsi_t4.index, data_bsi_t4.device_name)]

        # f3_ax1.plot(data_bsi_t4.index, np.zeros(len(data_bsi_t4)))
        # [f3_ax1.text(s, -1, ele_name, rotation=45, fontsize=7) for s, ele_name in zip(data_bsi_t4.index, data_bsi_t4.device_name)]

        f3_ax2.set_title("Up to 1st splitter")
        # f3_ax2.plot(data_time.loc[data_bsi_t2.device_name].T)

        norm = matplotlib.colors.Normalize(
            vmin=np.min(data_bsi_t2.index), vmax=np.max(data_bsi_t2.index)
        )

        c_m = matplotlib.cm.cool
        s_m = matplotlib.cm.ScalarMappable(cmap=c_m, norm=norm)
        s_m.set_array([])

        for i, ele in enumerate(data_bsi_t2.device_name):
            f3_ax2.plot(
                data_time.loc[ele], color=s_m.to_rgba(data_bsi_t2.index[i])
            )

        f3_ax2.set_xlabel("time / 20 ms")
        # f3_ax2.colorbar(s_m)

        # f3_ax31.set_title("T2")
        f3_ax31.bar(
            data_bsi_t2.index, data_bsi_t2.intensity, 3, color=p0[0].get_color()
        )
        f3_ax31.set_title("Intensity along line")
        f3_ax31.tick_params(
            axis="x",  # changes apply to the x-axis
            which="both",  # both major and minor ticks are affected
            # bottom=False,      # ticks along the bottom edge are off
            # top=False,         # ticks along the top edge are off
            labelbottom=False,
        )

        # f3_ax32.set_title("T4")
        f3_ax32.bar(
            data_bsi_t10.index[1:],
            data_bsi_t10.intensity[1:],
            0.6,
            color=p1[0].get_color(),
        )

        # f3_ax33.set_title("T6")
        f3_ax33.bar(
            data_bsi_t6.index, data_bsi_t6.intensity, 3, color=p2[0].get_color()
        )

        # plt.tight_layout()
    except Exception as e:
        print(e)


ani = FuncAnimation(fig3, make_plot, interval=1000)

plt.show()
