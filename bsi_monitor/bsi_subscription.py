from PyQt5.QtCore import QObject, pyqtSignal
import pyjapc
from cern_general_devices.bsi import BSI


class BSISubscriber(QObject):

    bsi_curve_signal = pyqtSignal(list)

    japc = pyjapc.PyJapc('SPS.USER.ALL')
    bsi = BSI(japc)
    counter = 0

    def __init__(self):
        super().__init__()

    def run(self):
        self.start_subscription()
        print('starting subscriptions')

    def callback(self, param, value, header):
        print(f'callback counter {self.counter}')
        self.counter = self.counter+1

        data = self.bsi.clean_data(value)
        data_time = self.bsi.clean_data_in_time(value)

        datas = [data, data_time]

        self.bsi_curve_signal.emit(datas)

    def start_subscription(self):

        self.japc.subscribeParam(self.bsi.bsis_var, self.callback, getHeader=True)
        self.japc.startSubscriptions()



