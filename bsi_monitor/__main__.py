import sys

from PyQt5.QtCore import QThread
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget
from bsi_monitor.controller import Controller
from bsi_monitor.gui.gui import Gui
from bsi_monitor.bsi_subscription import BSISubscriber


class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.controller = None
        self.setWindowTitle('North area BSI monitor')


        self.resize(2000, 1200)
        # self.setMaximumWidth(2400)

        self.thread2 = QThread()
        self.BSIsub = BSISubscriber()
        self.BSIsub.moveToThread(self.thread2)
        self.thread2.started.connect(self.BSIsub.run)
        self.thread2.start()

        # self.thread = QThread()
        # self.thread.started.connect(self.load_gui)
        # self.thread.start()
        self.load_gui()


    def load_gui(self):
        gui = Gui()
        self.setCentralWidget(gui)
        self.controller = Controller(gui, self.BSIsub)


if __name__ == '__main__':
    app: QApplication = QApplication(sys.argv)
    window: QWidget = MainWindow()
    window.show()
    app.exec()