import gc
import threading

import pyjapc
import numpy
from PyQt5.QtWidgets import QFrame, QVBoxLayout, QTabWidget, QWidget
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT
from matplotlib.figure import Figure
from matplotlib.patches import ConnectionPatch
import scipy.fftpack as fftpack
from collections import deque


class Gui(QFrame):

    def __init__(self):
        super().__init__()
        self.colours = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'indigo', 'chartreuse', 'olive', 'fuchsia', 'teal']

        self.japc = pyjapc.PyJapc("SPS.USER.ALL")
        # self.counter = 0

        mainvbox = QVBoxLayout()
        tab2vbox = QVBoxLayout()
        tab3vbox = QVBoxLayout()
        tab4vbox = QVBoxLayout()
        tab5vbox = QVBoxLayout()
        tab6vbox = QVBoxLayout()
        tab7vbox = QVBoxLayout()
        tab8vbox = QVBoxLayout()

        tabs = QTabWidget()
        tab2 = QWidget()
        tab3 = QWidget()
        tab4 = QWidget()
        tab5 = QWidget()
        tab6 = QWidget()
        tab7 = QWidget()
        tab8 = QWidget()

        tabs.addTab(tab2, "Spills and intensities")
        tabs.addTab(tab3, "Spill FFT")
        tabs.addTab(tab4, "Target intensity history")
        tabs.addTab(tab5, "T2, T4, T6 target spills")
        tabs.addTab(tab6, "Multiplicities")
        tabs.addTab(tab7, "Splitter losses")
        # tabs.addTab(tab8, "T4 to T10 ratio") # hidden until further developed

        self.bsi_t2_history = [deque(maxlen=100)]
        self.bsi_t4_history = [deque(maxlen=100)]
        self.bsi_t6_history = [deque(maxlen=100)]

        self.bsi_t10_history = [deque(maxlen=100), deque(maxlen=100), deque(maxlen=100)]
        self.bsi_t4_T10_ratio_history = [deque(maxlen=100)]

        self.sc2 = MplCanvas(self, width=50, height=30, dpi=100)
        # self.sc2.figure.set_constrained_layout(True)
        self.sc3 = MplCanvas(self, width=50, height=30, dpi=100)
        self.sc3.figure.set_constrained_layout(True)
        self.sc4 = MplCanvas(self, width=50, height=30, dpi=100)
        self.sc4.figure.set_constrained_layout(True)
        self.sc5 = MplCanvas(self, width=50, height=30, dpi=100)
        self.sc5.figure.set_constrained_layout(True)
        self.sc6 = MplCanvas(self, width=50, height=30, dpi=100)
        self.sc6.figure.set_constrained_layout(True)
        self.sc7 = MplCanvas(self, width=50, height=30, dpi=100)
        self.sc7.figure.set_constrained_layout(True)
        self.sc8 = MplCanvas(self, width=50, height=30, dpi=100)

        # self.sc8.figure.set_constrained_layout(True)

        self.toolbar2 = NavigationToolbar2QT(self.sc2, self)

        tab2vbox.addWidget(self.toolbar2)
        tab2vbox.addWidget(self.sc2)
        tab2vbox.addStretch()
        tab2.setLayout(tab2vbox)

        self.toolbar3 = NavigationToolbar2QT(self.sc3, self)

        tab3vbox.addWidget(self.toolbar3)
        tab3vbox.addWidget(self.sc3)
        tab3vbox.addStretch()
        tab3.setLayout(tab3vbox)

        self.toolbar4 = NavigationToolbar2QT(self.sc4, self)

        tab4vbox.addWidget(self.toolbar4)
        tab4vbox.addWidget(self.sc4)
        tab4vbox.addStretch()
        tab4.setLayout(tab4vbox)

        self.toolbar5 = NavigationToolbar2QT(self.sc5, self)

        tab5vbox.addWidget(self.toolbar5)
        tab5vbox.addWidget(self.sc5)
        tab5vbox.addStretch()
        tab5.setLayout(tab5vbox)

        self.toolbar6 = NavigationToolbar2QT(self.sc6, self)

        tab6vbox.addWidget(self.toolbar6)
        tab6vbox.addWidget(self.sc6)
        tab6vbox.addStretch()
        tab6.setLayout(tab6vbox)

        self.toolbar7 = NavigationToolbar2QT(self.sc7, self)

        tab7vbox.addWidget(self.toolbar7)
        tab7vbox.addWidget(self.sc7)
        tab7vbox.addStretch()
        tab7.setLayout(tab7vbox)

        self.toolbar8 = NavigationToolbar2QT(self.sc8, self)

        tab8vbox.addWidget(self.toolbar8)
        tab8vbox.addWidget(self.sc8)
        tab8vbox.addStretch()
        tab8.setLayout(tab8vbox)

        mainvbox.addWidget(tabs)
        self.setLayout(mainvbox)

        gs = self.sc2.figure.add_gridspec(24, 4, left=0.02, bottom=0.02, right=0.98, top=0.98, wspace=0.3, hspace=35)

        self.f2_tt20_spill = self.sc2.figure.add_subplot(gs[:12, 0:1])
        self.f2_tt20_spill.set_title('TT20 Spill')
        self.f2_tt20_intensity = self.sc2.figure.add_subplot(gs[12:, 0:1])
        self.f2_tt20_intensity.set_title('TT20 Intensity')

        self.f2_tt22_spill = self.sc2.figure.add_subplot(gs[:6, 1:2])
        self.f2_tt22_spill.set_title('TT22 Spill')
        self.f2_tt22_intensity = self.sc2.figure.add_subplot(gs[6:12, 1:2])
        self.f2_tt22_intensity.set_title('TT22 Intensity')

        self.f2_tt23_spill = self.sc2.figure.add_subplot(gs[:3, 2:4])
        self.f2_tt23_spill.set_title('TT23 Spill')
        self.f2_tt23_intensity = self.sc2.figure.add_subplot(gs[3:6, 2:4])
        self.f2_tt23_intensity.set_title('TT23 Intensity')

        self.f2_tt24_spill = self.sc2.figure.add_subplot(gs[6:9, 2:3])
        self.f2_tt24_spill.set_title('TT24 Spill')
        self.f2_tt24_intensity = self.sc2.figure.add_subplot(gs[9:12, 2:3])
        self.f2_tt24_intensity.set_title('TT24 Intensity')

        self.f2_tt25_spill = self.sc2.figure.add_subplot(gs[12:18, 1:4])
        self.f2_tt25_spill.set_title('TT25 Spill')
        self.f2_tt25_intensity = self.sc2.figure.add_subplot(gs[18:, 1:4])
        self.f2_tt25_intensity.set_title('TT25 Intensity')

        self.f2_p42_spill = self.sc2.figure.add_subplot(gs[6:9, 3:4])
        self.f2_p42_spill.set_title('P42 Spill')
        self.f2_p42_intensity = self.sc2.figure.add_subplot(gs[9:12, 3:4])
        self.f2_p42_intensity.set_title('P42 Intensity')

        self.sc2.draw_idle()

        self.f3_tt20 = self.sc3.figure.add_subplot(231)
        self.f3_tt22 = self.sc3.figure.add_subplot(232)
        self.f3_tt23 = self.sc3.figure.add_subplot(233)
        self.f3_tt24 = self.sc3.figure.add_subplot(234)
        self.f3_tt25 = self.sc3.figure.add_subplot(235)
        self.f3_p42 = self.sc3.figure.add_subplot(236)

        self.f4_t2_ax = self.sc4.figure.add_subplot(411)
        self.f4_t4_ax = self.sc4.figure.add_subplot(412, sharex=self.f4_t2_ax)
        self.f4_t6_ax = self.sc4.figure.add_subplot(413, sharex=self.f4_t2_ax)
        self.f4_t10_ax = self.sc4.figure.add_subplot(414, sharex=self.f4_t2_ax)

        self.f4_t2_ax.tick_params(axis="x", which="both", labelbottom =False)
        self.f4_t4_ax.tick_params(axis="x", which="both", labelbottom=False)
        self.f4_t6_ax.tick_params(axis="x", which="both", labelbottom=False)

        self.f5_ax1 = self.sc5.figure.add_subplot(131)

        self.f5_ax2 = self.sc5.figure.add_subplot(132, sharey=self.f5_ax1)
        self.f5_ax2.tick_params(
            axis="y",
            which="both",
            labelleft=False
        )

        self.f5_ax3 = self.sc5.figure.add_subplot(133, sharey=self.f5_ax1)
        self.f5_ax3.tick_params(
            axis="y",
            which="both",
            labelleft=False
        )

        self.f6_t2_ax = self.sc6.figure.add_subplot(321)
        self.f6_t4_ax = self.sc6.figure.add_subplot(323)
        self.f6_t6_ax = self.sc6.figure.add_subplot(325)
        self.f6_t2_multiplicity_ax = self.sc6.figure.add_subplot(322)
        self.f6_t4_multiplicity_ax = self.sc6.figure.add_subplot(324)
        self.f6_t6_multiplicity_ax = self.sc6.figure.add_subplot(326)

        self.f7_spl1_ax = self.sc7.figure.add_subplot(221)
        self.f7_spl2_ax = self.sc7.figure.add_subplot(223)
        self.f7_spl1_losses_ax = self.sc7.figure.add_subplot(222)
        self.f7_spl2_losses_ax = self.sc7.figure.add_subplot(224)

        self.f8_history = self.sc8.figure.add_subplot(211)
        self.f8_history_twin = self.f8_history.twinx()
        self.f8_raw = self.sc8.figure.add_subplot(223)
        self.f8_ratio = self.sc8.figure.add_subplot(224)

    def spillgraph(self, bsidata, timedata, spill_graph, fft_graph, title):
        for i, ele in enumerate(bsidata.device_name):
            target_corrected = (bsidata.loc[ele]['intensity'] / max(timedata.loc[ele])) * \
                               timedata.loc[ele]
            if i > 0:
                if i == len(bsidata.device_name) - 1:
                    # target_corrected = (bsidata.loc[ele]['intensity'] / max(timedata.loc[ele])) * \
                    #                    timedata.loc[ele]
                    spill_graph.fill_between(numpy.arange(0, len(timedata.loc[ele]) * 20, 20),
                                             numpy.zeros(len(timedata.loc[ele])),
                                             timedata.loc[ele],
                                             color=self.colours[i], alpha=0.2)

                spill_graph.fill_between(numpy.arange(0, len(timedata.loc[ele]) * 20, 20),
                                         timedata.loc[ele],
                                         timedata.loc[bsidata.device_name[i - 1]],
                                         color=self.colours[i - 1], alpha=0.2)
            if len(bsidata.device_name) == 1:
                spill_graph.fill_between(numpy.arange(0, len(timedata.loc[ele]) * 20, 20),
                                         numpy.zeros(len(timedata.loc[ele])),
                                         timedata.loc[ele],
                                         color=self.colours[i], alpha=0.2)

            spill_graph.plot(numpy.arange(0, len(timedata.loc[ele]) * 20, 20), timedata.loc[ele],
                             color=self.colours[i], label=ele)

            spill_graph.set_title(title)


            spill_graph.legend(prop={'size': 8})

            fft = fftpack.rfft(timedata.loc[ele])
            sample_freq = fftpack.rfftfreq(len(timedata.loc[ele]), d=0.02)
            fft_graph.plot(sample_freq, numpy.abs(fft), color=self.colours[i], label=ele)
            fft_graph.set_title(title + ' FFT')
            fft_graph.set_xlabel('Frequency (Hz)')
            fft_graph.set_ylabel('Amplitude (a.u.)')
            fft_graph.legend(prop={'size': 8})
            fft_graph.set_yscale("log")

    def intensity_graphs(self, bsidata, intensity_graph2, title):
        for i, (intensity, position) in enumerate(zip(bsidata.intensity, bsidata.s)):
            intensity_graph2.scatter(position, intensity, color=self.colours[i])
            intensity_graph2.set_title(title)

    def multiplicity_chart(self, data_time, device_up, device_down, rawchart, multiplicitychart, headerlabel):
        rawchart.plot(numpy.arange(0, len(data_time.loc[device_up]) * 20, 20), data_time.loc[device_up],
                      label=headerlabel + ' up: ' + device_up)
        rawchart.plot(numpy.arange(0, len(data_time.loc[device_down]) * 20, 20), data_time.loc[device_down],
                      label=headerlabel + ' down: ' + device_down)
        multiplicity = data_time.loc[device_down] / data_time.loc[device_up]
        multiplicitychart.plot(numpy.arange(0, len(multiplicity) * 20, 20), multiplicity,
                               label=headerlabel + ' multiplicity')
        rawchart.set_xlabel('Spill duration (ms)')
        rawchart.set_ylabel('Device counts (au)')
        multiplicitychart.set_xlabel('Spill duration (ms)')
        multiplicitychart.set_ylabel('Multiplicity - out/in (au)')
        rawchart.legend(prop={'size': 8})
        multiplicitychart.legend(prop={'size': 8})

    def splitter_chart(self, data_time, splitter_device_down_1, splitter_device_down_2, splitter_device_up, intensity_graph, losses_graph, splitter_name):
        intensity_graph.plot(numpy.arange(0, len(data_time.loc[splitter_device_up]) * 20, 20),
                             data_time.loc[splitter_device_up],
                             label=splitter_name + ' up: ' + splitter_device_up)
        intensity_graph.plot(numpy.arange(0, len(data_time.loc[splitter_device_down_1]) * 20, 20),
                             data_time.loc[splitter_device_down_1],
                             label=splitter_name + ' down 1 : ' + splitter_device_down_1)
        intensity_graph.plot(numpy.arange(0, len(data_time.loc[splitter_device_down_2]) * 20, 20),
                             data_time.loc[splitter_device_down_2],
                             label=splitter_name + ' down 2 : ' + splitter_device_down_2)
        splitter_1_losses = ((data_time.loc[splitter_device_up] - (
                data_time.loc[splitter_device_down_1] + data_time.loc[splitter_device_down_2])) / data_time.loc[
                                 splitter_device_up]) * 100
        losses_graph.plot(numpy.arange(0, len(splitter_1_losses) * 20, 20), splitter_1_losses,
                                    label=splitter_name + ' losses %')
        losses_graph.set_ylim(bottom=0, top=105)
        losses_graph.set_xlabel('Spill duration (ms)')
        losses_graph.set_ylabel('% losses wrt upstream')
        intensity_graph.set_xlabel('Spill duration (ms)')
        intensity_graph.set_ylabel('Counts (a.u.)')
        intensity_graph.legend(prop={'size': 8})
        losses_graph.legend(prop={'size': 8})

    def target_intensity_spills(self, data_bsi_target_upstream, data_time, axis, colour, target):
        for i, ele in enumerate(data_bsi_target_upstream.device_name):
            target_corrected = (data_bsi_target_upstream.loc[ele]['intensity'] / max(data_time.loc[ele])) * data_time.loc[ele]
            axis.plot(numpy.arange(0, len(target_corrected) * 20, 20), target_corrected, color=colour, label=ele)
            axis.fill_between(numpy.arange(0, len(target_corrected) * 20, 20), target_corrected,
                                     numpy.zeros(len(target_corrected)), color=colour, alpha=0.2)
            axis.set_title(target + ' Spill')
            axis.set_xlabel('Spill duration (ms)')
            axis.set_ylabel('integrated intesity * time resolved data (charges)')
            axis.spines['bottom'].set_color(colour)
            axis.spines['top'].set_color(colour)
            axis.spines['right'].set_color(colour)
            axis.spines['left'].set_color(colour)
            axis.title.set_color(colour)
            axis.legend(prop={'size': 8})

    def append_to_history(self, data_bsi, bsi_history):
        print("data_bsi ")
        print(len(data_bsi))
        print("history ")
        print(len(bsi_history))
        for i, bsi in enumerate(bsi_history):
            print(bsi)
            print("history ")
            print(bsi_history[i])
            print("data ")
            print(data_bsi.iloc[i])
            bsi_history[i].append(data_bsi.iloc[i]["intensity"])

    def historyplot(self, data_bsi, bsi_history, graph):

        print(data_bsi)

        for i, bsi in enumerate(bsi_history):
            graph.plot(numpy.arange(-(len(bsi)), 0), bsi, label=data_bsi.device_name[i])
            graph.legend()
            graph.set_ylabel('integrated intesity')

    def set_value_curve(self, value):
        # print(value)

        gc.collect()

        data = value[0]
        data_time = value[1]

        data_bsi = data[data["device_name"].str.contains("BSI")]
        print(data_bsi)

        data_bsi_tt20 = data_bsi[data_bsi["device_name"].str.contains(".210", regex=False)]
        data_bsi_tt20 = data_bsi_tt20.sort_values(by=['s'])
        data_bsi_tt22 = data_bsi[data_bsi["device_name"].str.contains(".22", regex=False)]
        data_bsi_tt22 = data_bsi_tt22.sort_values(by=['s'])
        data_bsi_tt23 = data_bsi[data_bsi["device_name"].str.contains(".230705", regex=False) | data_bsi["device_name"].str.contains(".230949", regex=False)]
        data_bsi_tt23 = data_bsi_tt23.sort_values(by=['s'])
        data_bsi_tt24 = data_bsi[data_bsi["device_name"].str.contains(".240610", regex=False) | data_bsi["device_name"].str.contains(".241149", regex=False)]
        data_bsi_tt24 = data_bsi_tt24.sort_values(by=['s'])
        data_bsi_tt25 = data_bsi[data_bsi["device_name"].str.contains(".251010", regex=False) | data_bsi["device_name"].str.contains(".251247", regex=False)]
        data_bsi_tt25 = data_bsi_tt25.sort_values(by=['s'])
        data_bsi_t10 = data_bsi[data_bsi["device_name"].str.contains(".04", regex=False)]
        # data_bsi_t10 = data_bsi[data_bsi["device_name"].str.contains(".04")]
        data_bsi_t10 = data_bsi_t10.sort_values(by=['s'])

        data_bsi_t2_up = data_bsi[data_bsi["device_name"].str.contains(".230949", regex=False)]
        data_bsi_t2_up = data_bsi_t2_up.sort_values(by=['s'])
        data_bsi_t4_up = data_bsi[data_bsi["device_name"].str.contains(".241149", regex=False)]
        data_bsi_t4_up = data_bsi_t4_up.sort_values(by=['s'])
        data_bsi_t6_up = data_bsi[data_bsi["device_name"].str.contains(".251247", regex=False)]
        data_bsi_t6_up = data_bsi_t6_up.sort_values(by=['s'])
        data_bsi_t10_up = data_bsi[data_bsi["device_name"].str.contains(".045914", regex=False)]
        # data_bsi_t10_up = data_bsi[data_bsi["device_name"].str.contains(".045836", regex=False)]
        data_bsi_t10_up = data_bsi_t10_up.sort_values(by=['s'])




        print("T2 up BSI")
        self.append_to_history(data_bsi_t2_up, self.bsi_t2_history)
        print("T4 up BSI")
        self.append_to_history(data_bsi_t4_up, self.bsi_t4_history)
        print("T6 up BSI")
        self.append_to_history(data_bsi_t6_up, self.bsi_t6_history)
        # print("T10 up BSI")
        # self.append_to_history(data_bsi_t10, self.bsi_t10_history)

        # for i, bsi_t10 in enumerate(self.bsi_t10_history):
        #     self.bsi_t10_history[i].append(data_bsi_t10.iloc[i]["intensity"])

        # for i, bsi_t2_up in enumerate(self.bsi_t2_history):
        #     self.bsi_t2_history[i].append(data_bsi_t2_up.iloc[i]["intensity"])

        # for i, bsi_t4_up in enumerate(self.bsi_t4_history):
        #     self.bsi_t4_history[i].append(data_bsi_t4_up.iloc[i]["intensity"])

        # for i, bsi_t6_up in enumerate(self.bsi_t6_history):
        #     self.bsi_t6_history[i].append(data_bsi_t6_up.iloc[i]["intensity"])

        # for i, bsi_T4_T10_ratio in enumerate(self.bsi_t4_T10_ratio_history):
        #
        #     self.bsi_t4_T10_ratio_history[i].append(data_bsi_t10_up.iloc[0]["intensity"]/data_bsi_t4_up.iloc[0]["intensity"])

        target_t2_ex = self.japc.getParam(
            "rmi://virtual_sps/T2.REF/Reference#exponent", timingSelectorOverride=""
        )
        target_t4_ex = self.japc.getParam(
            "rmi://virtual_sps/T4.REF/Reference#exponent", timingSelectorOverride=""
        )
        target_t6_ex = self.japc.getParam(
            "rmi://virtual_sps/T6.REF/Reference#exponent", timingSelectorOverride=""
        )
        target_t2 = self.japc.getParam(
            "rmi://virtual_sps/T2.REF/Reference#value", timingSelectorOverride=""
        )
        target_t4 = self.japc.getParam(
            "rmi://virtual_sps/T4.REF/Reference#value", timingSelectorOverride=""
        )
        target_t6 = self.japc.getParam(
            "rmi://virtual_sps/T6.REF/Reference#value", timingSelectorOverride=""
        )

        target_t2 = float(target_t2) * 10 ** int(target_t2_ex)
        target_t4 = float(target_t4) * 10 ** int(target_t4_ex)
        target_t6 = float(target_t6) * 10 ** int(target_t6_ex)

        self.clear_graphs()

        tt20_spill_thread = threading.Thread(
            target=self.spillgraph(data_bsi_tt20, data_time, self.f2_tt20_spill, self.f3_tt20, 'TT20 Spill'),
            name='tt20_spill_thread')
        tt20_intensity_thread = threading.Thread(
            target=self.intensity_graphs(data_bsi_tt20, self.f2_tt20_intensity,
                                         'TT20 Intensity'), name='tt20_intensity_thread')

        tt22_spill_thread = threading.Thread(
            target=self.spillgraph(data_bsi_tt22, data_time, self.f2_tt22_spill, self.f3_tt22, 'TT22 Spill'),
            name='tt22_spill_thread')
        tt22_intensity_thread = threading.Thread(
            target=self.intensity_graphs(data_bsi_tt22, self.f2_tt22_intensity,
                                         'TT22 Intensity'), name='tt22_intensity_thread')

        tt23_spill_thread = threading.Thread(
            target=self.spillgraph(data_bsi_tt23, data_time, self.f2_tt23_spill, self.f3_tt23, 'TT23 Spill'),
            name='tt22_spill_thread')
        tt23_intensity_thread = threading.Thread(
            target=self.intensity_graphs(data_bsi_tt23, self.f2_tt23_intensity,
                                         'TT23 Intensity'), name='tt22_intensity_thread')

        tt24_spill_thread = threading.Thread(
            target=self.spillgraph(data_bsi_tt24, data_time, self.f2_tt24_spill, self.f3_tt24, 'TT24 Spill'),
            name='tt24_spill_thread')
        tt24_intensity_thread = threading.Thread(
            target=self.intensity_graphs(data_bsi_tt24, self.f2_tt24_intensity,
                                         'TT24 Intensity'), name='tt24_intensity_thread')

        tt25_spill_thread = threading.Thread(
            target=self.spillgraph(data_bsi_tt25, data_time, self.f2_tt25_spill, self.f3_tt25, 'TT25 Spill'),
            name='tt25_spill_thread')
        tt25_intensity_thread = threading.Thread(
            target=self.intensity_graphs(data_bsi_tt25, self.f2_tt25_intensity,
                                         'TT25 Intensity'), name='tt25_intensity_thread')

        p42_spill_thread = threading.Thread(
            target=self.spillgraph(data_bsi_t10, data_time, self.f2_p42_spill, self.f3_p42, 'P42 Spill'),
            name='p42_spill_thread')
        p42_intensity_thread = threading.Thread(
            target=self.intensity_graphs(data_bsi_t10, self.f2_p42_intensity, 'P42 Intensity'),
            name='p42_intensity_thread')

        # Multiplicities threads

        t2_device_up = 'BSI.230949'
        t2_device_down = 'BSI.230950'
        t4_device_up = 'BSI.241149'
        t4_device_down = 'BSI.241150'
        t6_device_up = 'BSI.251247'
        t6_device_down = 'BSI.251248'

        t2_multiplicity_thread = threading.Thread(
            target=self.multiplicity_chart(data_time, t2_device_up, t2_device_down, self.f6_t2_ax,
                                           self.f6_t2_multiplicity_ax,
                                           'T2'),
            name='t2_multiplicity_thread')
        t4_multiplicity_thread = threading.Thread(
            target=self.multiplicity_chart(data_time, t4_device_up, t4_device_down, self.f6_t4_ax,
                                           self.f6_t4_multiplicity_ax,
                                           'T4'),
            name='t4_multiplicity_thread')
        t6_multiplicity_thread = threading.Thread(
            target=self.multiplicity_chart(data_time, t6_device_up, t6_device_down, self.f6_t6_ax,
                                           self.f6_t6_multiplicity_ax,
                                           'T6'),
            name='t6_multiplicity_thread')

        # Splitter losses threads

        # spl1_device_up = 'BSIB.210279'
        spl1_device_up = 'BSIA.220412'
        spl1_device_down_t2_t4 = 'BSIA.220412'
        spl1_device_down_t6 = 'BSI.251010'

        spl2_device_up = 'BSIA.220412'
        spl2_device_down_t2 = 'BSI.230705'
        spl2_device_down_t4 = 'BSI.240610'

        splitter_1_losses_thread = threading.Thread(
            target=self.splitter_chart(data_time, spl1_device_down_t2_t4, spl1_device_down_t6, spl1_device_up,
                            self.f7_spl1_ax, self.f7_spl1_losses_ax, 'Splitter 1'),
            name='splitter_1_losses_thread')

        splitter_2_losses_thread = threading.Thread(
            target=self.splitter_chart(data_time, spl2_device_down_t2, spl2_device_down_t4, spl2_device_up,
                            self.f7_spl2_ax, self.f7_spl2_losses_ax, 'Splitter 2'),
            name='splitter_2_losses_thread')

        # Target intensities

        t2_intensity_spill_thread = threading.Thread(
            target=self.target_intensity_spills(data_bsi_t2_up, data_time, self.f5_ax1, 'blue', 'T2'),
            name='t2_intensity_spill_thread')
        t4_intensity_spill_thread = threading.Thread(
            target=self.target_intensity_spills(data_bsi_t4_up, data_time, self.f5_ax2,  'green', 'T4'),
            name='t4_intensity_spill_thread')
        t6_intensity_spill_thread = threading.Thread(
            target=self.target_intensity_spills(data_bsi_t6_up, data_time, self.f5_ax3,  'magenta', 'T6'),
            name='t6_intensity_spill_thread')

        # starting threads
        tt20_spill_thread.start()
        tt20_intensity_thread.start()
        tt22_spill_thread.start()
        tt22_intensity_thread.start()
        tt23_spill_thread.start()
        tt23_intensity_thread.start()
        tt24_spill_thread.start()
        tt24_intensity_thread.start()
        tt25_spill_thread.start()
        tt25_intensity_thread.start()
        p42_spill_thread.start()
        p42_intensity_thread.start()
        t2_multiplicity_thread.start()
        t4_multiplicity_thread.start()
        t6_multiplicity_thread.start()
        splitter_1_losses_thread.start()
        splitter_2_losses_thread.start()
        t2_intensity_spill_thread.start()
        t4_intensity_spill_thread.start()
        t6_intensity_spill_thread.start()

        # wait until all threads finish
        tt20_spill_thread.join()
        tt20_intensity_thread.join()
        tt22_spill_thread.join()
        tt22_intensity_thread.join()
        tt23_spill_thread.join()
        tt23_intensity_thread.join()
        tt24_spill_thread.join()
        tt24_intensity_thread.join()
        tt25_spill_thread.join()
        tt25_intensity_thread.join()
        p42_spill_thread.join()
        p42_intensity_thread.join()
        t2_multiplicity_thread.join()
        t4_multiplicity_thread.join()
        t6_multiplicity_thread.join()
        splitter_1_losses_thread.join()
        splitter_2_losses_thread.join()
        t2_intensity_spill_thread.join()
        t4_intensity_spill_thread.join()
        t6_intensity_spill_thread.join()

        # BSI T10 history plots

        # self.historyplot(data_bsi_t10, self.bsi_t10_history, self.f4_t10_ax)
        self.historyplot(data_bsi_t2_up, self.bsi_t2_history, self.f4_t2_ax)
        self.historyplot(data_bsi_t4_up, self.bsi_t4_history, self.f4_t4_ax)
        self.historyplot(data_bsi_t6_up, self.bsi_t6_history, self.f4_t6_ax)


        # for i, bsi_t10 in enumerate(self.bsi_t10_history):
        #     self.f4_t10_ax.plot(numpy.arange(-(len(bsi_t10)), 0), bsi_t10, label=data_bsi_t10.device_name[i])
        # self.f4_t10_ax.legend()
        # self.f4_t10_ax.set_xlabel('Past cycles')
        # self.f4_t10_ax.set_ylabel('integrated intesity')

        # for i, bsi_t2_up in enumerate(self.bsi_t2_history):
        #     self.f4_t2_ax.plot(numpy.arange(-(len(bsi_t2_up)), 0), bsi_t2_up, label=data_bsi_t2_up.device_name[i])
        # self.f4_t2_ax.legend()
        # self.f4_t2_ax.set_ylabel('integrated intesity')

        # for i, bsi_t4_up in enumerate(self.bsi_t4_history):
        #     self.f4_t4_ax.plot(numpy.arange(-(len(bsi_t4_up)), 0), bsi_t4_up, label=data_bsi_t4_up.device_name[i])
        # self.f4_t4_ax.legend()
        # self.f4_t4_ax.set_ylabel('integrated intesity')

        # for i, bsi_t6_up in enumerate(self.bsi_t6_history):
        #     self.f4_t6_ax.plot(numpy.arange(-(len(bsi_t6_up)), 0), bsi_t6_up, label=data_bsi_t6_up.device_name[i])
        # self.f4_t6_ax.legend()
        # self.f4_t6_ax.set_ylabel('integrated intesity')

        self.f4_t10_ax.set_xlabel('Past cycles')

        self.f4_t2_ax.set_title('T2 target intensity history')
        self.f4_t4_ax.set_title('T4 target intensity history')
        self.f4_t6_ax.set_title('T6 target intensity history')
        self.f4_t10_ax.set_title('T10 target intensity history')

        # T4 to T10 ratio

        T4_up_device_name = "BSI.241149"
        T10_up_device_name = "T10-BSI.045914"

        self.f8_raw.plot(data_time.loc[T4_up_device_name])
        # self.f8_raw.plot(data_time.loc[T10_up_device_name])
        self.f8_raw.set_title('pickles')
        # self.f8_ratio.plot(data_time.loc[T4_up_device_name]/data_time.loc[T10_up_device_name])
        self.f8_ratio.set_title('pickles2')

        self.f8_history.plot(numpy.arange(-(len(self.bsi_t4_history[0])), 0), self.bsi_t4_history[0], label='T4 upstream'+T4_up_device_name)
        self.f8_history.plot(numpy.arange(-(len(self.bsi_t10_history[2])), 0), self.bsi_t10_history[2], label='T10 upstream'+T10_up_device_name)
        self.f8_history_twin.plot(numpy.arange(-(len(self.bsi_t4_T10_ratio_history[0])), 0), self.bsi_t4_T10_ratio_history[0],
                             label='ponko', color='red')
        self.f8_history_twin.yaxis.label.set_color('red')
        self.f8_history_twin.tick_params(axis='y', colors='red')
        # self.f8_history.plot(numpy.arange(-(len(self.bsi_t10_history[2])), 0), self.bsi_t4_history[0]/self.bsi_t10_history[2], label='T10 upstream' + T10_up_device_name, color='red')

        # Target intensity references

        t2_start = (self.f5_ax1.get_xlim()[0], target_t2)
        t2_end = (self.f5_ax3.get_xlim()[1], target_t2)
        if self.f5_ax1.get_ylim()[1] < target_t2:
            self.f5_ax1.set_ylim(top=target_t2 + (target_t2 / 10))
        con = ConnectionPatch(xyA=t2_start, xyB=t2_end, coordsA="data", coordsB="data",
                              axesA=self.f5_ax1, axesB=self.f5_ax3, ls=(0, (5, 10)), color="blue")
        self.sc5.figure.add_artist(con)

        t4_start = (self.f5_ax1.get_xlim()[0], target_t4)
        t4_end = (self.f5_ax3.get_xlim()[1], target_t4)
        if self.f5_ax1.get_ylim()[1] < target_t4:
            self.f5_ax1.set_ylim(top=target_t4 + (target_t4 / 10))
        con2 = ConnectionPatch(xyA=t4_start, xyB=t4_end, coordsA="data", coordsB="data",
                               axesA=self.f5_ax1, axesB=self.f5_ax3, ls=(0, (5, 10)), color="green")
        self.sc5.figure.add_artist(con2)

        t2_start = (self.f5_ax1.get_xlim()[0], target_t6)
        t2_end = (self.f5_ax3.get_xlim()[1], target_t6)
        if self.f5_ax1.get_ylim()[1] < target_t6:
            self.f5_ax1.set_ylim(top=target_t6 + (target_t6 / 10))
        con3 = ConnectionPatch(xyA=t2_start, xyB=t2_end, coordsA="data", coordsB="data",
                               axesA=self.f5_ax1, axesB=self.f5_ax3, ls=(0, (5, 10)), color="magenta")
        self.sc5.figure.add_artist(con3)

        self.sc2.draw_idle()
        self.sc3.draw_idle()
        self.sc4.draw_idle()
        self.sc5.draw_idle()
        self.sc6.draw_idle()
        self.sc7.draw_idle()
        self.sc8.draw_idle()

    def clear_graphs(self):

        [ax.cla() for ax in self.sc2.figure.get_axes()]
        [ax.cla() for ax in self.sc3.figure.get_axes()]
        [ax.cla() for ax in self.sc4.figure.get_axes()]
        [ax.cla() for ax in self.sc5.figure.get_axes()]
        [ax.cla() for ax in self.sc6.figure.get_axes()]
        [ax.cla() for ax in self.sc7.figure.get_axes()]
        [ax.cla() for ax in self.sc8.figure.get_axes()]
        self.sc5.figure.artists.clear()


class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=14, height=10, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        super(MplCanvas, self).__init__(fig)
